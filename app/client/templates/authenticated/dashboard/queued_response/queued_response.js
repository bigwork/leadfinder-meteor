/*****************************************************************************/
/* QueuedResponse: Event Handlers */
/*****************************************************************************/
Template.QueuedResponse.events({
});

/*****************************************************************************/
/* QueuedResponse: Helpers */
/*****************************************************************************/
Template.QueuedResponse.helpers({
});

/*****************************************************************************/
/* QueuedResponse: Lifecycle Hooks */
/*****************************************************************************/
Template.QueuedResponse.created = function () {
};

Template.QueuedResponse.rendered = function () {
};

Template.QueuedResponse.destroyed = function () {
};
