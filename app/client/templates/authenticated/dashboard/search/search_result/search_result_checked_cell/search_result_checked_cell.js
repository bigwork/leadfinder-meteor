/*****************************************************************************/
/* SearchResultCheckedCell: Event Handlers */
/*****************************************************************************/
Template.SearchResultCheckedCell.events({
    'change #queuedResponseCheckbox': function(event) {
        event.preventDefault();

        SearchResult.update(this._id,{$set:{queued:event.target.checked}});

    }
});

/*****************************************************************************/
/* SearchResultCheckedCell: Helpers */
/*****************************************************************************/
Template.SearchResultCheckedCell.helpers({
    currentPost: function () {

        var post = SearchResult.findOne(this._id);


        return post;

    }
});

/*****************************************************************************/
/* SearchResultCheckedCell: Lifecycle Hooks */
/*****************************************************************************/
Template.SearchResultCheckedCell.created = function () {
};

Template.SearchResultCheckedCell.rendered = function () {

};

Template.SearchResultCheckedCell.destroyed = function () {
};
