/*****************************************************************************/
/* SearchResult: Event Handlers */
/*****************************************************************************/
Template.SearchResult.events({
    'click tbody > tr': function (event) {
        var dataTable = $(event.target).closest('table').DataTable();
        var rowData = dataTable.row(event.currentTarget).data();

        Session.set('CURRENT_POST_ID', rowData._id);
    }, 'change #queuedResponseCheckbox': function (event) {
        event.preventDefault();

        SearchResult.update(Session.get('CURRENT_POST_ID'), {$set: {queued: event.target.checked}});

        if (event.target.checked && !Session.get("SHOW_QUEUED_RESPONSES")) {
            Session.set('CURRENT_POST_ID', null);
        }


    }, 'change #hiddenCheckbox': function (event) {
        event.preventDefault();

        SearchResult.update(Session.get('CURRENT_POST_ID'), {$set: {hidden: event.target.checked}});

        if (!Session.get("SHOW_HIDDEN_RESPONSES")) {
            Session.set('CURRENT_POST_ID', null);
        }

    }, 'change #showQueuedCheckbox': function (event) {
        event.preventDefault();

        Session.set("SHOW_QUEUED_RESPONSES", event.target.checked);
        Session.set('CURRENT_POST_ID', null);

    }, 'change #showHiddenCheckbox': function (event) {
        event.preventDefault();

        Session.set("SHOW_HIDDEN_RESPONSES", event.target.checked);
        Session.set('CURRENT_POST_ID', null);

    },
    'click #doSearch': function (event, template) {
        event.preventDefault();

        //SearchController.runSearch(this.search._id);

        Router.go('runSearch', {_id: this.search._id});

    },
    'click #doEdit': function (event, template) {
        event.preventDefault();


        Modal.show('UpsertSearch', this.search);


    },
    'click #queueResponse': function (event, template) {
        event.preventDefault();


    },
    'click #hidePost': function (event, template) {
        event.preventDefault();


    },
    'click #queueAll': function (event, template) {
        event.preventDefault();

        var table = $('#PostListTable').DataTable();
        table.rows().iterator('row', function (context, index) {
            var post = this.row(index).data();

            SearchResult.update(post._id, {$set: {queued: true}});

        });
    },
    'click #unQueueAll': function (event, template) {
        event.preventDefault();

        var table = $('#PostListTable').DataTable();
        table.rows().iterator('row', function (context, index) {
            var post = this.row(index).data();

            SearchResult.update(post._id, {$set: {queued: false}});

        });
    }
});


/*****************************************************************************/
/* SearchResult: Helpers */
/*****************************************************************************/
Template.SearchResult.helpers({
    response: function () {
        var responseText;

        var post = SearchResult.findOne(Session.get('CURRENT_POST_ID'));
        var search = Search.findOne(post.searchId);
        var response = ResponseTemplate.findOne({code: search.responseTemplateCode});


        var parseSubject = _.template(response.subject);
        var parseBody = _.template(response.body);

        var subject = parseSubject({title: post.title, url: post.url});
        var body = parseBody({title: post.title, url: post.url});

        return {subject: subject, body: body, attachment: response.attachment};
    },
    currentPost: function () {
        if (Session.get('CURRENT_POST_ID')) {
            var post = SearchResult.findOne(Session.get('CURRENT_POST_ID'));


            return post;
        }
    },
    postListSelector: function () {
        var searchId = this.search._id;

        var selector = {};

        selector.$and = [];

        selector.$and.push({responded: {$ne: true}});
        selector.$and.push({email: {$exists: true}});
        selector.$and.push({searchId: searchId});
        selector.$and.push({hidden: {$ne: true}});

        if (!Session.get("SHOW_QUEUED_RESPONSES")) {
            selector.$and.push({queued: {$ne: true}});
        }

        if (!Session.get("SHOW_HIDDEN_RESPONSES")) {

        }

        return selector;
        //return {$and: [{hidden: {$ne: true}}, {responded: {$ne: true}}, {searchId: searchId}]};
    },
    showQueuedResponses: function () {
        return Session.get("SHOW_QUEUED_RESPONSES");
    },
    showHiddenResponses: function () {
        return Session.get("SHOW_HIDDEN_RESPONSES");
    }
});


/*****************************************************************************/
/* SearchResult: Lifecycle Hooks */
/*****************************************************************************/
Template.SearchResult.created = function () {

    Session.set("SHOW_QUEUED_RESPONSES", false);
    Session.set("SHOW_HIDDEN_RESPONSES", false);
};

Template.SearchResult.rendered = function () {

    var table = $('#PostListTable').DataTable();
    var firstRow = table.row(0).data();
    Session.set('CURRENT_POST_ID', firstRow._id);

    $('#PostListTable').on('draw.dt', function () {

        //$("#PostListTable thead").remove();
        //var firstRow = table.row(0).data();
        //Session.set('CURRENT_POST_ID',firstRow._id);
    });
};

Template.SearchResult.destroyed = function () {
};
