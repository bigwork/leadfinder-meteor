/*****************************************************************************/
/* SearchResultFetcher: Event Handlers */
/*****************************************************************************/
Template.SearchResultFetcher.events({
    'click #updateSearches': function (event, template) {
        event.preventDefault();


        //$('#loadingSpinner').show();


    }
});

/*****************************************************************************/
/* SearchResultFetcher: Helpers */
/*****************************************************************************/
Template.SearchResultFetcher.helpers({});

Template.SearchResultFetcher.updateAllSearches = function () {


};




Template.SearchResultFetcher.logMessage = function (message) {
    $('#resultTextArea').append('------------------------\n');
    $('#resultTextArea').append(message + '\n');

    $('#resultTextArea').scrollTop($('#resultTextArea')[0].scrollHeight);
};

/*****************************************************************************/
/* SearchResultFetcher: Lifecycle Hooks */
/*****************************************************************************/
Template.SearchResultFetcher.created = function () {

    Meteor.subscribe('AllSearchResults', {
        onReady: function () {
            var searchResults = SearchResult.find({}).fetch();

            searchResults.forEach(function (post) {


                if (!post.hidden && !post.responded && !post.email && post.url) {

                    var rand = Math.round(Math.random() * (2500 - 500)) + 500;
                    setTimeout(function () {

                        //Template.SearchResultFetcher.parsePost(post)


                    }, rand);

                }

            });
        }
    });
};

Template.SearchResultFetcher.rendered = function () {

    $('#loadingSpinner')
        .hide()  // Hide it initially
        .ajaxStart(function () {
            $(this).show();
        })
        .ajaxStop(function () {
            $(this).hide();
        })
    ;


};

Template.SearchResultFetcher.destroyed = function () {
};
