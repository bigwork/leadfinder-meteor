/*****************************************************************************/
/* RunSearch: Event Handlers */
/*****************************************************************************/
Template.RunSearch.events({
    'click #doSearch': function (event, template) {
        event.preventDefault();

        RunSearchController.runSearch(this.search._id);

        /*
         SearchController.runSearch(this.search._id,function (err, result) {

         console.log('result: ' + JSON.stringify(result));

         });
         */

        /*
         Meteor.call('/app/runSearch', this.search._id, function (err, result) {

         console.log('result: ' + JSON.stringify(result));

         });
         */
    }
});

/*****************************************************************************/
/* RunSearch: Helpers */
/*****************************************************************************/
Template.RunSearch.helpers({
});

/*****************************************************************************/
/* RunSearch: Lifecycle Hooks */
/*****************************************************************************/
Template.RunSearch.created = function () {
};

Template.RunSearch.rendered = function () {
};

Template.RunSearch.destroyed = function () {
};
