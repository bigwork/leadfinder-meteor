/*****************************************************************************/
/* UpsertSearch: Event Handlers */
/*****************************************************************************/
Template.UpsertSearch.events({
    'click #newSearch': function (event, template) {
        event.preventDefault();




    },
    'click #doDelete': function (event, template) {
        event.preventDefault();


        // TODO: Delete...
        Search.remove(this._id);


    }
});

/*****************************************************************************/
/* UpsertSearch: Helpers */
/*****************************************************************************/
Template.UpsertSearch.helpers({

    isInsert: function () {

        return this._id == undefined;
    }
});

/*****************************************************************************/
/* UpsertSearch: Lifecycle Hooks */
/*****************************************************************************/
Template.UpsertSearch.created = function () {



};

Template.UpsertSearch.rendered = function () {


};

Template.UpsertSearch.destroyed = function () {
};
