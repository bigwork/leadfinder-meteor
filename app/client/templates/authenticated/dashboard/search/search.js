/*****************************************************************************/
/* Search: Event Handlers */
/*****************************************************************************/
Template.Search.events({
    'click tbody > tr': function (event) {
        var dataTable = $(event.target).closest('table').DataTable();
        var rowData = dataTable.row(event.currentTarget).data();

        Router.go('searchResult', {_id: rowData._id});
    },
    'click #newSearch': function (event, template) {
        event.preventDefault();


        Modal.show('UpsertSearch');

    },
    'click #processQueue': function (event, template) {
        event.preventDefault();


        Meteor.call('/app/processQueuedResponses',function(error,result){
            if (error) {
                console.log('error: ' + error.message);
            } else {
                console.log('result: ' + JSON.stringify(result));
            }
        });

    }
});

/*****************************************************************************/
/* Search: Helpers */
/*****************************************************************************/
Template.Search.helpers({

    queuedSelector: function () {

        var selector = {};

        selector.$and = [];

        selector.$and.push({responded: {$ne: true}});
        selector.$and.push({email: {$exists: true}});

        selector.$and.push({hidden: {$ne: true}});
        selector.$and.push({queued: true});
        selector.$and.push({responded: {$ne: true}});

        return selector;

    }
});


/*****************************************************************************/
/* Search: Lifecycle Hooks */
/*****************************************************************************/
Template.Search.created = function () {
};

Template.Search.rendered = function () {
};

Template.Search.destroyed = function () {
};
