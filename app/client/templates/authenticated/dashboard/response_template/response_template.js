/*****************************************************************************/
/* ResponseTemplate: Event Handlers */
/*****************************************************************************/
Template.ResponseTemplate.events({
});

/*****************************************************************************/
/* ResponseTemplate: Helpers */
/*****************************************************************************/
Template.ResponseTemplate.helpers({
});

/*****************************************************************************/
/* ResponseTemplate: Lifecycle Hooks */
/*****************************************************************************/
Template.ResponseTemplate.created = function () {
};

Template.ResponseTemplate.rendered = function () {
};

Template.ResponseTemplate.destroyed = function () {
};
