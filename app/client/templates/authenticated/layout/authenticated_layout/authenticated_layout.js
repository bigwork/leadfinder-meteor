/*****************************************************************************/
/* AuthenticatedLayout: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.AuthenticatedLayout.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.AuthenticatedLayout.helpers({
  /*
   * Example:
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* AuthenticatedLayout: Lifecycle Hooks */
/*****************************************************************************/
Template.AuthenticatedLayout.created = function () {
};

Template.AuthenticatedLayout.rendered = function () {

  App.init();
  ReadyDashboard.init();
};

Template.AuthenticatedLayout.destroyed = function () {
};