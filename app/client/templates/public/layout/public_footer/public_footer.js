/*****************************************************************************/
/* PublicFooter: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.PublicFooter.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.PublicFooter.helpers({
  appVersion: function () {
        return 'v' + AppVersion;
      }
});

/*****************************************************************************/
/* PublicFooter: Lifecycle Hooks */
/*****************************************************************************/
Template.PublicFooter.created = function () {
};

Template.PublicFooter.rendered = function () {
};

Template.PublicFooter.destroyed = function () {
};