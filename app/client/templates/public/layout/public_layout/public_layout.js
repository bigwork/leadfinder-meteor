/*****************************************************************************/
/* PublicLayout: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.PublicLayout.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.PublicLayout.helpers({
  /*
   * Example:
   *  items: function () {
   *    return Items.find();
   *  }
   */
});

/*****************************************************************************/
/* PublicLayout: Lifecycle Hooks */
/*****************************************************************************/
Template.PublicLayout.created = function () {
};

Template.PublicLayout.rendered = function () {

  // Let's initialize the AppUI javascript helpers
  PublicApp.init();

};

Template.PublicLayout.destroyed = function () {
};