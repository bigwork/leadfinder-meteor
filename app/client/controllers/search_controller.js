/*

// Client
SearchController.minWaitMs = 1000;
SearchController.maxWaitMs = 10000;

SearchController.setupSearchQueue = function () {

    Meteor.call('/app/getCityUrls', function (err, result) {

        if (!err) {
            SearchController.cityUrls = result;

            Meteor.subscribe('CityData', {
                onReady: function () {


                }
            });

            Meteor.subscribe('search', {
                onReady: function () {

                    SearchController.searchObserver = Search.find({}).observe(
                        {
                            added: function (search) {
                                //console.log('Added Search: ' + JSON.stringify(search._id));
                                // TODO: Add some sort of time interrogation here...
                                //SearchController.runSearch(search._id);
                            }
                        }
                    );

                }
            });

            Meteor.subscribe('AllSearchResults', {
                onReady: function () {

                    SearchController.searchResultObserver = SearchResult.find().observe(
                        {
                            added: function (post) {
                                //console.log('Added Search Result: ' + JSON.stringify(post._id));
                                if (!post.hidden && !post.responded && !post.email && post.url) {

                                    var rand = Math.round(Math.random() * (SearchController.maxWaitMs - SearchController.minWaitMs)) + SearchController.minWaitMs;
                                    setTimeout(function () {

                                        //Template.SearchResultFetcher.parsePost(post)
                                        SearchController.parsePost(post);

                                    }, rand);

                                }
                            }
                        }
                    );
                }
            });


        }

    });


}

SearchController.searchObserver = {};

SearchController.searchResultObserver = {};

SearchController.cityQueue = [];

SearchController.queueCityUrl = function (cityUrl) {


};


SearchController.runSearch = function (searchId, callback) {

    try {


        console.log('runSearch: ' + searchId);

        var search = Search.findOne(searchId);
        var cityDatas = CityData.find({}).fetch();

        cityDatas.forEach(function (cityData) {

            var cityUrl = cityData.url;
            console.log('running search for city: ' + cityUrl);

            var parser = document.createElement('a');
            parser.href = cityUrl;
            var city = parser.hostname.split('.')[0];

            search.categories.forEach(function (category) {

                //var options = {searchId: searchId, cityUrl: cityUrl, city: city, category: category, keyword: keyword};

                var rand = Math.round(Math.random() * (SearchController.maxWaitMs - SearchController.minWaitMs)) + SearchController.minWaitMs;
                setTimeout(function () {

                    SearchController.getSearchResults(searchId, cityUrl, city, category, search.searchString, function (err, result) {

                        if (callback) {
                            callback(err, result);
                        }

                    });

                }, rand);

            });

            return;


        });

    } catch (ex) {

        Template.SearchResultFetcher.logMessage(ex.message);
    }

}

SearchController.getSearchResults = function (searchId, cityUrl, city, category, searchString, callback) {

    try {


        //console.log('getSearchResults: ' + searchId);
        var message = 'Fetching: ' + cityUrl;
        Template.SearchResultFetcher.logMessage(message);
        var url = cityUrl + '/search/' + category + '?query=' + encodeURIComponent(searchString) + '&format=rss'


        SearchController.getRss(url, function (result) {

            if (result.entries) {

                result.entries.forEach(function (listing) {
                    //console.log(listing);

                    var listingData = {
                        category: category,
                        title: listing.title,
                        description: listing.content,
                        url: listing.link,
                        searchId: searchId,
                        date: listing.publishedDate,
                        city: city,
                        searchString: searchString
                    };

                    //SearchResult.upsert({url: listing.link}, {$set: listingData});

                    Meteor.call('/app/upsertSearchResult', {url: listing.link}, listingData, function (err, result) {
                        console.log('result: ' + JSON.stringify(result));


                        if (callback) {
                            callback(err, result);
                        }

                    });



                });

            }

        });

    } catch (ex) {

        Template.SearchResultFetcher.logMessage(ex.message);
    }
};

SearchController.parsePost = function (post) {

    console.log('parsePost: ' + post._id);
    //var post = SearchResult.findOne(postId);
    try {


        if (!post.email) {

            HTTP.call('GET', post.url, function (error, result) {
                if (error) {
                    var message = post._id + ' ERROR: ' + error.message;
                    Template.SearchResultFetcher.logMessage(message);
                } else {
                    //console.log('post html: ' + result.content);

                    //$('#postHTMLDiv').html(result.content);
                    var replyhref = $(result.content).find('#replylink').attr('href');

                    if (!replyhref) {
                        var message = 'searchResult: ' + post._id + ' had no reply button!';
                        Template.SearchResultFetcher.logMessage(message);
                        SearchResult.update(post._id, {$set: {hidden: true}});
                        //Meteor.call('/app/updateSearchResult', {_id: post._id, hidden: true});
                    } else {

                        console.log('replyhref: ' + replyhref);

                        //Meteor.call('/app/updateCityAbbreviation', post.url, replyhref);

                        var parser = document.createElement('a');
                        parser.href = post.url;
                        var replyLink = parser.protocol + '//' + parser.hostname + replyhref;
                        HTTP.call('GET', replyLink, function (error, result) {
                            if (error) {
                                var message = post._id + ' ERROR: ' + error.message;
                                Template.SearchResultFetcher.logMessage(message);
                            } else {
                                //console.log('post html: ' + result.content);

                                //$('#postHTMLDiv').html(result.content);
                                var email = $(result.content).find('.anonemail').text();

                                if (email) {
                                    var message = 'parsed email: ' + email;
                                    Template.SearchResultFetcher.logMessage(message);
                                    //SearchResult.update(post._id, {$set: {email: email}});
                                    SearchResult.update(post._id, {$set: {email: email}});
                                    //Meteor.call('/app/updateSearchResult', {_id: post._id, email: email});
                                } else {
                                    //SearchResult.update(post._id, {$set: {hidden: true}});
                                    var message = post._id + ' had no email!';
                                    //Template.SearchResultFetcher.logMessage(message);
                                    SearchResult.update(post._id, {$set: {hidden: true}});
                                    //Meteor.call('/app/updateSearchResult', {_id: post._id, hidden: true});
                                }


                            }
                        })
                    }
                }
            })
        }

    } catch (ex) {

        Template.SearchResultFetcher.logMessage(ex.message);
    }
};

SearchController.parseEmailFromPost = function () {

}


SearchController.getRss = function (url, callback) {
    console.log('getRss: ' + url);

    try {

        $.ajax({
            url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
            dataType: 'json',
            success: function (data) {
                callback(data.responseData.feed);
            }
        });

    } catch (ex) {

        Template.SearchResultFetcher.logMessage(ex.message);
    }
};

*/