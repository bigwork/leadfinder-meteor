/*****************************************************************************/
/* Server Only Methods */
/*****************************************************************************/
Meteor.methods({
    '/app/processQueuedResponses': function () {


        /*
         var selector = {};

         selector.$and = [];

         selector.$and.push({responded: {$ne: true}});
         selector.$and.push({email: {$exists: true}});

         selector.$and.push({hidden: {$ne: true}});
         selector.$and.push({queued: true});
         selector.$and.push({responded: {$ne: true}});
         */
        var selector = {$and: [
            {responded: {$ne: true}},
            {email: {$exists: true}},
            {hidden: {$ne: true}},
            {queued: true},
            {responded: {$ne: true}}
        ]};
        var queuedResponses = SearchResult.find(selector,{limit:10}).fetch();

        console.log('Processing ' + queuedResponses.length + ' Queued Responses');


        queuedResponses.forEach(function (post) {

            console.log('post: ' + post.title);

            var result = Meteor.call('sendEmail',
                'vincil@bigworkindustries.com',
                'vincil.bishop@vbishop.com',
                post.responseSubject,
                post.responseBody,
                post.responseAttachment, post._id);

            console.log('result: ' + JSON.stringify(result));

        });

        return true;

    }

});
