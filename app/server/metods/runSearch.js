/**
 * Created by vincilbishop on 4/27/15.
 */

Meteor.methods({
    /*
     * Example:
     *
     * '/app/items/insert': function (item) {
     * }
     */

    '/app/runSearch': function (searchId) {

        //console.log('searchId: ' + searchId);

        var search = Search.findOne(searchId);

        SearchController.cityUrls.forEach(function (cityUrl) {

            var urlParts = UrlUtil.parse(cityUrl);

            var city = urlParts.host.split('.')[0];


            search.categories.forEach(function(category){

                search.keywords.forEach(function(keyword){

                    var options = {searchId:searchId,cityUrl:cityUrl,city:city,category:category,keyword:keyword};

                    console.log('adding task with options: ' + JSON.stringify(options));

                    Cue.addTask('searchKeywordCategory', {isAsync:true, unique:true}, {options:options});
                    //Meteor.call('/app/runSearchKeywordCategory',searchId,cityUrl,city,category,keyword);
                });

            });

            return;


        });



    },
    '/app/runSearchKeywordCategory': function (searchId,cityUrl,city,category,keyword) {

        var url = cityUrl + '/search/' + category + '?query=' + keyword + '&format=rss'

        var result = Scrape.feed(url);

        result.items.forEach(function (listing) {
            //console.log(listing);

            var listingData = {
                category: category,
                title: listing.title,
                description: listing.description,
                url: listing.link,
                searchId: searchId,
                date: listing.pubDate,
                city: city
            };

            SearchResult.upsert({url: listing.link}, {$set: listingData});

            console.log('result: ' + JSON.stringify(result));
        });

    }
});