/**
 * Created by vincilbishop on 4/27/15.
 */

Meteor.methods({
    /*
     * Example:
     *
     * '/app/items/insert': function (item) {
     * }
     */

    '/app/parsePost': function (searchResultId) {

        var post = SearchResult.findOne(searchResultId);

        //var result = Scrape.url(post.url);

        console.log('/app/parsePost:'+searchResultId);

        if (post && !post.email && post.url) {

            var future = new Future();
            xray(post.url)
                .use(XRayPhantom())
                .delay(500, 1000)
                .select({
                    $root: '.replylink',
                    link: 'a[href]'
                })
                .run(function (err, element) {
                    if (!err) {

                        //console.log('element: ' + JSON.stringify(element));

                        if (element && element.link) {
                            xray(element.link)
                                .use(XRayPhantom())
                                .delay(500, 1000)
                                .select('.anonemail')
                                .run(function (err, email) {
                                    if (!err) {

                                        //console.log('element: ' + JSON.stringify(email));
                                        future.return(email);
                                    } else {
                                        console.log('error retrieving post details: ' + err.message);
                                        future.throw(err);
                                    }
                                });
                        } else {
                            future.return(null);

                        }

                        //future.return(element);
                    } else {
                        console.log('error retrieving post details: ' + err.message);
                        future.throw(err);
                    }
                });

            var email = future.wait();



            if (email) {
                console.log('updating searchResult: ' +searchResultId + ' with email: ' + email);
                SearchResult.update(searchResultId, {$set: {email: email}});
            } else {
                SearchResult.update(searchResultId, {$set: {hidden: true}});
            }

        } else {
            console.log('No post URL!');
            SearchResult.update(searchResultId, {$set: {hidden: true}});
        }

        return;

    }
});