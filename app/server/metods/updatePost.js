/**
 * Created by vincilbishop on 4/29/15.
 */


Meteor.methods({
    '/app/updateSearchResult': function (searchResult) {

        var searchResultId = searchResult._id;
        delete searchResult._id;
        return SearchResult.update(searchResultId, {$set: searchResult});
    },
    '/app/upsertSearchResult': function (selector,searchResult) {

        var existingPost = SearchResult.findOne({title:searchResult.title});

        if (existingPost == undefined) {
            //var searchResultId = searchResult._id;
            delete searchResult._id;
            return SearchResult.upsert(selector, {$set: searchResult});
        }

    }


});