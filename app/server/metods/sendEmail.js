/**
 * Created by vincilbishop on 5/4/15.
 */

Meteor.methods({
    sendEmail: function (to, from, subject, text, attachment, post) {
        //check([to, from, subject, text, attachment], [String]);

        // Let other method calls from the same client start running,
        // without waiting for the email sending to complete.
        this.unblock();

        if (post.responded != true) {
            //var nodemailer = Nodemailer;
            //"smtp://vincil%40bigworkindustries.com:Como3stas@box893.bluehost.com:465"
            var transport = nodeMailer.createTransport(smtpTransport({
                host: 'smtp.mailgun.org',
                port: 25,
                connectionTimeout: 20000,
                debug: true,
                auth: {
                    user: 'postmaster@bigworkindustries.com',
                    pass: 'bf40ab54b926beb025810a239b3502ce'
                }
            }));

            var base = process.env.PWD;

            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: from, // sender address
                to: to, // list of receivers
                subject: subject, // Subject line
                html: text, // html body,
                headers:{"X-Mailgun-Campaign-Id":"bigworkcl"},
                attachments: [
                    {   // file on disk as an attachment
                        filename: 'vincil-bishop-mobile-resume-march-2015.pdf',
                        path: base + '/private/' + attachment, // stream this file,
                        contentType: 'application/pdf'
                    }
                ]
            };


            // Random sleep
            var minWaitSecs = 1000;
            var maxWaitSecs = 10000;
            var rand = Math.round(Math.random() * (maxWaitSecs - minWaitSecs)) + minWaitSecs;
            Sleep(rand);

            var future = new Future();
// send mail with defined transport object
            try {
                transport.sendMail(mailOptions, Meteor.bindEnvironment(function (error, info) {

                    transport.close();

                    if (error) {
                        console.log('*** Message error: ' + error.message);
                        SearchResult.update({_id: post._id}, {$set: {errored: true, errorMessage: error.message}});
                        future.return(error);
                    } else {
                        console.log('Message sent: ' + info.response);

                        if (!info.rejected || info.rejected.length == 0) {
                            SearchResult.update({_id: post._id}, {$set: {responded: true, sendResultMessage:info.response}});
                        } else {

                            console.log("********** REJECTED **********");
                        }

                        future.return(info);
                    }

                }));

            } catch (ex) {
                console.log(ex.message);
                transport.close();
                future.return(ex);
            }

            return future.wait();
        }
        /*
         var result = Email.send({
         to: to
         from: from,
         subject: subject,
         text: text
         });
         */

//console.log('mail result: ' + JSON.stringify(result));
    }
});
