/**
 * Created by vincilbishop on 4/28/15.
 */


Meteor.methods({
    '/app/refreshCraigslistCities': function () {

        /*
         var cityAbbreviations = CityAbbreviation.find({},{"_id":0}).fetch();
         var base = process.env.PWD
         console.log('base dir: ' + base);

         var cleanedCityAbbrev = _.map(cityAbbreviations,function(item){

         delete item._id;
         return item;
         });

         fs.writeFileSync(base + '/cityAbbreviations.json',JSON.stringify(cleanedCityAbbrev));
         */

        var future = new Future();
        xray('https://geo.craigslist.org/iso/us')
            .use(XRayPhantom())
            .select(['blockquote a[href]'])
            .run(function (err, elements) {
                if (!err) {

                    future.return(elements);
                } else {
                    future.throw(err);
                }
            });

        var elements = future.wait();

        SearchController.cityUrls = elements;

        var base = process.env.PWD
        console.log('base dir: ' + base);

        fs.writeFileSync(base + '/urls.txt',JSON.stringify(elements));

        console.log('Craigslist cities refreshed!');

        /*
        console.log('elements: ' + JSON.stringify(elements));

        elements.forEach(function(url){

            console.log(url);

        });
        */

    }
});