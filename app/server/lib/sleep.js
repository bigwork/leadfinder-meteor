
Sleep = function sleep(ms) {
    console.log("zzzzzzz Sleeping: " + ms);
    var fiber = Fiber.current;
    setTimeout(function() {
        fiber.run();
    }, ms);
    Fiber.yield();
}
