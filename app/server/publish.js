/**
 * Meteor.publish('items', function (param1, param2) {
 *  this.ready();
 * });
 */


Meteor.publish('search', function (/* args */) {
  return Search.find();
});


Meteor.publish('SearchResult', function (searchId) {
  return SearchResult.find({$and:[{hidden:{$ne:true}},{responded:{$ne:true}},{searchId:searchId}]});
});

Meteor.publish('AllNewSearchResults', function () {
  return SearchResult.find({$and:[{hidden:{$ne:true}},{responded:{$ne:true}},{email:{$exists:false}}]});
});


Meteor.publish('ResponseTemplate', function (/* args */) {
  return ResponseTemplate.find();
});

Meteor.publish('QueuedResponse', function (/* args */) {
  return QueuedResponse.find();
});

Meteor.publish('CityData', function (/* args */) {
  return CityData.find();
});