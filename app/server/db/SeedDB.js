/**
 * Created by vincilbishop on 4/27/15.
 */

Meteor.startup(function () {

    var testUser = Meteor.users.findOne({'emails.address': 'vincil.bishop@vbishop.com'});

    if (!testUser) {
        Accounts.createUser({email: 'vincil.bishop@vbishop.com', password: '#password01'});
    }

    var objects = JSON.parse(Assets.getText("db/searches.json"));

    objects.forEach(function (object) {
        var exists = typeof Search.findOne({searchString: object.searchString}) === 'object';

        if (!exists) {

            Search.insert(object);
        }
    });

    objects = JSON.parse(Assets.getText("db/responseTemplates.json"));

    objects.forEach(function (object) {
        var exists = typeof ResponseTemplate.findOne({code: object.code}) === 'object';

        if (!exists) {

            ResponseTemplate.insert(object);
        }
    });

    // Cities


    var cityUrls = JSON.parse(Assets.getText('craigslist_city_urls.json'));
    var cityAbbrevs = JSON.parse(Assets.getText('cityAbbreviations.json'));
    if (CityData.find({}).count() == 0) {

        cityUrls.forEach(function (cityUrl) {

            var postUrlParts = UrlUtil.parse(cityUrl);
            var city =  postUrlParts.host.split('.')[0];

            var cityData = {};
            cityData.city = city;
            cityData.url = cityUrl;

            var cityAbbrev = _.find(cityAbbrevs,function(abbrev){
                return abbrev.city == city;
            });

            if (cityAbbrev) {
                cityData.abbreviation = cityAbbrev.abbreviation
            }

            CityData.insert(cityData);

        });

    }


});