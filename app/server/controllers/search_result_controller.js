SearchResultController.observeChanges = function () {

    Cue.start();
    Cue.maxTasksAtOnce = 1;

    Cue.addJob('getResultDetails', {retryOnError: true, maxMs:20000}, function (task, done) {
        //console.log('doing ' + task.jobName + ' with ' + task.data.searchResultId);
        Meteor.call('/app/parsePost',task.data.searchResultId);
        done();
    });



    SearchResultController.observerHandle = SearchResult.find().observe(
        {
            added: function (document) {
                console.log('Added Search Result: ' + JSON.stringify(document._id));
                Cue.addTask('getResultDetails', {isAsync:true, unique:true}, {searchResultId: document._id});
            }
        }
    );

};