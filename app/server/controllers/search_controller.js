SearchController.cityUrls = [];

SearchController.setupSearchQueue = function () {

    SearchController.cityUrls = JSON.parse(Assets.getText('craigslist_city_urls.txt'));

    //console.log('SearchController.cityUrls: ' + JSON.stringify(SearchController.cityUrls));

    Cue.start();

    Cue.addJob('searchKeywordCategory', {retryOnError: true, maxMs: 20000}, function (task, done) {
        //console.log('doing ' + task.jobName + ' with ' + task.data.searchResultId);
        Meteor.call('/app/runSearchKeywordCategory', task.data.options.searchId, task.data.options.cityUrl, task.data.options.city, task.data.options.category, task.data.options.keyword);
        done();
    });


}