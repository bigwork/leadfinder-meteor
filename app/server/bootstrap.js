Future = Npm.require ('fibers/future');
Fiber = Npm.require ('fibers');
UrlUtil = Npm.require('url');
Craigslist = Meteor.npmRequire('node-craigslist');
XRayPhantom = Meteor.npmRequire('x-ray-phantom');
nodeMailer = Meteor.npmRequire('nodemailer');
smtpTransport = Meteor.npmRequire('nodemailer-smtp-transport');

SearchResultObserverHandle = {};

Meteor.startup(function () {

    //var fiber = new Fiber()

    Fiber(function(){
        var selector = {$and: [
            {responded: {$ne: true}},
            {email: {$exists: true}},
            {hidden: {$ne: true}},
            {queued: true},
            {responded: {$ne: true}}
        ]};


        var respondedEmails = [
            "alex@biblioso.com",
            "zksxc-5007568293@job.craigslist.org",
            "sn5sz-5000989863@job.craigslist.org",
            "bhtg3-5028286122@gigs.craigslist.org",
            "53xrc-5016071963@job.craigslist.org",
            "tnmqh-5022121545@job.craigslist.org",
            "7t87w-5021529649@job.craigslist.org",
            "nina@cloudspace.com",
            "mhebert@localytics.com",
            "nfrfz-5028433323@gigs.craigslist.org",
            "veena.krishnamurthy@akvelon.com",
            "cs@bodhi5.com",
            "chrisnorton@funnyordie.com",
            "TimT@rsh.com",
            "qnjkc-5023807837@job.craigslist.org",
            "3zbq3-4958180015@gigs.craigslist.org",
            "thomas@powershiftmobile.com"
        ];


        /*
        var respondedEmails = [
            "vincil.bishop@vbishop.com"
        ];
        */

        respondToPost = function (post) {

            var responded = _.indexOf(respondedEmails,post.email) != -1;

            SearchResult.update(post._id,{$set:{responded:true}});

            if (responded) {
                console.log(">>>>>>>>>> found a responded email!");
            }



            post.responded = responded;

            if (post.responded != true) {
                Meteor.call('sendEmail',
                    post.email,
                    'vincil@bigworkindustries.com',
                    post.responseSubject,
                    post.responseBody,
                    post.responseAttachment, post,function(error,result){
                        if (error) {
                            console.log('response error: ' + error.message);
                        } else {
                            console.log('result: ' + JSON.stringify(result));
                        }

                    });
            }
        }

        SearchResultObserverHandle = SearchResult.find(selector).observe(
            {
                added: function (document) {
                    console.log('>>> responding to: ' + JSON.stringify(document._id));
                    respondToPost(document);
                },
                changed: function (newDocument, oldDocument) {
                    respondToPost(newDocument);
                }
            }
        );
    }).run()


});