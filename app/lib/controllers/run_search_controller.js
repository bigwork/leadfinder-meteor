RunSearchController = RouteController.extend({
  subscriptions: function () {
    // set up the subscriptions for the route and optionally
    // wait on them like this:
    //
    // this.subscribe('item', this.params._id).wait();
    //
    // "Waiting" on a subscription does not block. Instead,
    // the subscription handle is added to a reactive list
    // and when all items in this list are ready, this.ready()
    // returns true in any of your route functions.

    this.subscribe('CityData', {
      onReady: function () {


      }
    });

    this.subscribe('search', {
      onReady: function () {

        RunSearchController.searchObserver = Search.find({}).observe(
            {
              added: function (search) {
                //console.log('Added Search: ' + JSON.stringify(search._id));
                // TODO: Add some sort of time interrogation here...
                //RunSearchController.runSearch(search._id);
              }
            }
        );

      }
    });

    this.subscribe('AllNewSearchResults', {
      onReady: function () {

        RunSearchController.searchResultObserver = SearchResult.find().observe(
            {
              added: function (post) {
                //console.log('Added Search Result: ' + JSON.stringify(post._id));
                if (!post.hidden && !post.responded && !post.email && post.url) {

                  var rand = Math.round(Math.random() * (RunSearchController.maxWaitMs - RunSearchController.minWaitMs)) + RunSearchController.minWaitMs;
                  setTimeout(function () {

                    //Template.SearchResultFetcher.parsePost(post)
                    RunSearchController.parsePost(post);

                  }, rand);

                }
              }
            }
        );
      }
    });

    this.subscribe('ResponseTemplate').wait();
  },

  data: function () {
    // return a global data context like this:
    return {search: Search.findOne(this.params._id)};
  },

  action: function () {
    // You can create as many action functions as you'd like.
    // This is the primary function for running your route.
    // Usually it just renders a template to a page. But it
    // might also perform some conditional logic. Override
    // the data context by providing it as an option in the
    // last parameter.
    this.render('RunSearch');
  }
});


// Client
RunSearchController.minWaitMs = 1000;
RunSearchController.maxWaitMs = 10000;

RunSearchController.setupSearchQueue = function () {

  Meteor.call('/app/getCityUrls', function (err, result) {

    if (!err) {
      RunSearchController.cityUrls = result;




    }

  });

  /*
   $(document).on("customEvent",{foo:"foo"},function(event,arg1,arg2){
   $(self).dequeue("cityUrlQueue");
   });


   $( document ).trigger( "customEvent", [ "bim", "baz" ] );
   */
}

RunSearchController.searchObserver = {};

RunSearchController.searchResultObserver = {};

RunSearchController.cityQueue = [];

RunSearchController.queueCityUrl = function (cityUrl) {

  /*
   $(div).queue("cityUrlQueue", function() {
   console.log(cityUrl);
   var self = this;
   setTimeout(function() {

   }, 1000);
   });
   */
};


RunSearchController.runSearch = function (searchId, callback) {

  try {


    console.log('runSearch: ' + searchId);

    var search = Search.findOne(searchId);
    var cityDatas = CityData.find({}).fetch();

    cityDatas.forEach(function (cityData) {

      var cityUrl = cityData.url;
      console.log('running search for city: ' + cityUrl);

      var parser = document.createElement('a');
      parser.href = cityUrl;
      var city = parser.hostname.split('.')[0];

      search.categories.forEach(function (category) {

        //var options = {searchId: searchId, cityUrl: cityUrl, city: city, category: category, keyword: keyword};

        var rand = Math.round(Math.random() * (RunSearchController.maxWaitMs - RunSearchController.minWaitMs)) + RunSearchController.minWaitMs;
        setTimeout(function () {

          RunSearchController.getSearchResults(searchId, cityUrl, city, category, search.searchString, function (err, result) {

            if (callback) {
              callback(err, result);
            }

          });

        }, rand);

      });

      return;


    });

  } catch (ex) {

    Template.SearchResultFetcher.logMessage(ex.message);
  }

}

RunSearchController.getSearchResults = function (searchId, cityUrl, city, category, searchString, callback) {

  try {


    //console.log('getSearchResults: ' + searchId);
    var message = 'Fetching: ' + cityUrl;
    Template.SearchResultFetcher.logMessage(message);
    var url = cityUrl + '/search/' + category + '?query=' + encodeURIComponent(searchString) + '&format=rss'


    RunSearchController.getRss(url, function (result) {

      if (result.entries) {

        result.entries.forEach(function (listing) {
          //console.log(listing);

          var listingData = {
            category: category,
            title: listing.title,
            description: listing.content,
            url: listing.link,
            searchId: searchId,
            date: listing.publishedDate,
            city: city,
            searchString: searchString
          };

          //SearchResult.upsert({url: listing.link}, {$set: listingData});

          Meteor.call('/app/upsertSearchResult', {url: listing.link}, listingData, function (err, result) {
            console.log('result: ' + JSON.stringify(result));


            if (callback) {
              callback(err, result);
            }

          });



        });

      }

    });

  } catch (ex) {

    Template.SearchResultFetcher.logMessage(ex.message);
  }
};

RunSearchController.parsePost = function (post) {

  console.log('parsePost: ' + post._id);
  //var post = SearchResult.findOne(postId);
  try {


    if (!post.email) {

      HTTP.call('GET', post.url, function (error, result) {
        if (error) {
          var message = post._id + ' ERROR: ' + error.message;
          Template.SearchResultFetcher.logMessage(message);
        } else {
          //console.log('post html: ' + result.content);

          //$('#postHTMLDiv').html(result.content);
          var replyhref = $(result.content).find('#replylink').attr('href');

          if (!replyhref) {
            var message = 'searchResult: ' + post._id + ' had no reply button!';
            Template.SearchResultFetcher.logMessage(message);
            SearchResult.update(post._id, {$set: {hidden: true}});
            //Meteor.call('/app/updateSearchResult', {_id: post._id, hidden: true});
          } else {

            console.log('replyhref: ' + replyhref);

            //Meteor.call('/app/updateCityAbbreviation', post.url, replyhref);

            var parser = document.createElement('a');
            parser.href = post.url;
            var replyLink = parser.protocol + '//' + parser.hostname + replyhref;
            HTTP.call('GET', replyLink, function (error, result) {
              if (error) {
                var message = post._id + ' ERROR: ' + error.message;
                Template.SearchResultFetcher.logMessage(message);
              } else {
                //console.log('post html: ' + result.content);

                //$('#postHTMLDiv').html(result.content);
                var email = $(result.content).find('.anonemail').text();

                if (email) {
                  var message = 'parsed email: ' + email;
                  Template.SearchResultFetcher.logMessage(message);
                  //SearchResult.update(post._id, {$set: {email: email}});
                  SearchResult.update(post._id, {$set: {email: email}});
                  RunSearchController.parseResponseTemplate(post);
                  //Meteor.call('/app/updateSearchResult', {_id: post._id, email: email});
                } else {
                  //SearchResult.update(post._id, {$set: {hidden: true}});
                  var message = post._id + ' had no email!';
                  //Template.SearchResultFetcher.logMessage(message);
                  SearchResult.update(post._id, {$set: {hidden: true}});
                  //Meteor.call('/app/updateSearchResult', {_id: post._id, hidden: true});
                }


              }
            })
          }
        }
      })
    }

  } catch (ex) {

    Template.SearchResultFetcher.logMessage(ex.message);
  }
};

RunSearchController.parseResponseTemplate = function (post) {

  var search = Search.findOne(post.searchId);
  var response = ResponseTemplate.findOne({code: search.responseTemplateCode});


  var parseSubject = _.template(response.subject);
  var parseBody = _.template(response.body);

  var subject = parseSubject({title: post.title, url: post.url});
  var body = parseBody({title: post.title, url: post.url});
  SearchResult.update(post._id, {$set: {responseSubject: subject, responseBody: body, responseAttachment: response.attachment}});

}

RunSearchController.parseEmailFromPost = function () {

}


RunSearchController.getRss = function (url, callback) {
  console.log('getRss: ' + url);

  try {

    $.ajax({
      url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
      dataType: 'json',
      success: function (data) {
        callback(data.responseData.feed);
      }
    });

  } catch (ex) {

    Template.SearchResultFetcher.logMessage(ex.message);
  }
};