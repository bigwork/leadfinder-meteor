ResponseTemplate = new Mongo.Collection('response_template');

ResponseTemplate.attachSchema (
    new SimpleSchema ({

      code: {
        type: String
      },
      subject: {
        type: String,
        optional: true
      },
      body: {
        type:String,
        optional:true
      },
      attachment: {
        type: String,
        optional:true
      }
    })
);


if (Meteor.isServer) {
  ResponseTemplate.allow({
    insert: function (userId, doc) {
      return false;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function (userId, doc) {
      return false;
    }
  });

  ResponseTemplate.deny({
    insert: function (userId, doc) {
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });
}
