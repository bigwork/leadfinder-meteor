Search = new Mongo.Collection('search');

Search.attachSchema (
    new SimpleSchema ({

      searchString: {
        type:String,
        optional:true
      },
      categories: {
        type: [String],
        optional:true
      },
      responseTemplateCode: {
        type: String,
        optional: true
      }
    })
);


if (Meteor.isServer) {
  Search.allow({
    insert: function (userId, doc) {
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });

  Search.deny({
    insert: function (userId, doc) {
      return false;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function (userId, doc) {
      return false;
    }
  });
}
