CityData = new Mongo.Collection('city_data');

CityData.attachSchema (
    new SimpleSchema ({

      city: {
        type: String
      },
      abbreviation: {
        type:String,
        optional:true
      },
      url: {
        type:String
      }
    })
);


if (Meteor.isServer) {
  CityData.allow({
    insert: function (userId, doc) {
      return false;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function (userId, doc) {
      return false;
    }
  });

  CityData.deny({
    insert: function (userId, doc) {
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });
}
