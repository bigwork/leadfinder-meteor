SearchResult = new Mongo.Collection('search_result');

SearchResult.attachSchema(
    new SimpleSchema({

        title: {
            type: String
        },
        description: {
            type: String,
            optional: true
        },
        searchId: {
            type: String
        },
        category: {
            type: String,
            optional: true
        },
        city: {
            type: String,
            optional: true
        },
        location: {
            type: String,
            optional: true
        },
        url: {
            type: String
        },
        body: {
            type: String,
            optional: true
        },
        email: {
            type: String,
            optional: true
        },
        responded: {
            type: Boolean,
            defaultValue: false,
            optional: true
        },
        hidden: {
            type: Boolean,
            defaultValue: false,
            optional: true
        },
        date: {
            type: Date,
            optional: true
        },
        queued: {
            type: Boolean,
            defaultValue: false,
            optional: true
        },
        responseTemplateCode: {
            type: String,
            optional: true
        },
        searchString: {
            type: String,
            optional: true
        },
        responseSubject: {
            type: String,
            optional: true
        },
        responseBody: {
            type: String,
            optional: true
        },
        responseAttachment: {
            type: String,
            optional: true
        },
        errored: {
            type: Boolean,
            defaultValue: false,
            optional: true
        },
        errorMessage: {
            type: String,
            optional: true
        },
        sendResultMessage : {
            type: String,
            optional: true
        }
    })
);

if (Meteor.isServer) {
    SearchResult.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    SearchResult.deny({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}
