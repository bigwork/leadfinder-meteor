/**
 * Created by vincilbishop on 4/27/15.
 */

Meteor.startup(function () {
    TabularTables.postList = new Tabular.Table({
        name: "Search Results",
        collection: SearchResult,
        iDisplayLength: 25,
        bLengthChange:false,
        dom:'<"top"fi><"bottom"p>',
        order:[[1,"desc"]],

        columns: [
            {data: "title", title:"Title"},
            {data: "date", title:"Post Date", type:"date", render: function (val, type, doc) {
                if (val instanceof Date) {
                    return moment(val).format("MM/DD/YYYY HH:mm:ss");
                }
            }},
            {
                tmpl: Meteor.isClient && Template.SearchResultCheckedCell
            }
        ]
    });

})

