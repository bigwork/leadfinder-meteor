/**
 * Created by vincilbishop on 4/30/15.
 */

Meteor.startup(function () {
    TabularTables.ResponseTemplates = new Tabular.Table({
        name: "Response Templates",
        collection: ResponseTemplate,
        iDisplayLength: 100,
        bLengthChange:false,
        dom:'<"top"i><"bottom"p>',
        columns: [
            {data: "code", title:"Code"},
            {data: "subject", title:"Subject"}
        ]
    });

})