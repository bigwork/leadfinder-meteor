/**
 * Created by vincilbishop on 4/27/15.
 */

Meteor.startup(function () {
    TabularTables.QueuedResponses = new Tabular.Table({
        name: "Queued Responses Results",
        collection: SearchResult,
        iDisplayLength: 25,
        bLengthChange:false,
        dom:'<"top"fip><"bottom">',
        order:[[1,"desc"]],

        columns: [
            {data: "title", title:"Title"},
            {data: "date", title:"Post Date", type:"date", render: function (val, type, doc) {
                if (val instanceof Date) {
                    return moment(val).format("MM/DD/YYYY HH:mm:ss");
                }
            }}
        ]
    });

})

