/**
 * Created by vincilbishop on 4/27/15.
 */

Meteor.startup(function(){
    TabularTables.Searches = new Tabular.Table({
        name:"Search List",
        collection:Search,
        iDisplayLength: 100,
        bLengthChange:false,
        dom:'<"top"i><"bottom"p>',
        columns: [
            {data:"searchString", title:"Search String"},
            {data:"categories", title:"Categories"}
        ]
    });

})

