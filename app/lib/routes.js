Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/',function () {
  this.redirect('/dashboard');
});


Router.route('/dashboard', {
  name: 'dashboard',
  onBeforeAction: function () {
    AccountsEntry.signInRequired(this);
  }
});

Router.route('search', {
  name: 'search',
  controller: 'SearchController',
  action: 'action',
  where: 'client',
  onBeforeAction: function () {
    AccountsEntry.signInRequired(this);
  }
});

Router.route('search_result/:_id', {
  name: 'searchResult',
  controller: 'SearchResultController',
  action: 'action',
  where: 'client',
  onBeforeAction: function () {
    AccountsEntry.signInRequired(this);
  }
});

Router.route('settings', {
  name: 'settings',
  controller: 'SettingsController',
  action: 'action',
  where: 'client',
  onBeforeAction: function () {
    AccountsEntry.signInRequired(this);
  }
});

Router.route('queued_responses', {
  name: 'queuedResponses',
  controller: 'QueuedResponsesController',
  action: 'action',
  where: 'client'
});

Router.route('response_template', {
  name: 'responseTemplate',
  controller: 'ResponseTemplateController',
  action: 'action',
  where: 'client'
});

Router.route('queued_response', {
  name: 'queuedResponse',
  controller: 'QueuedResponseController',
  action: 'action',
  where: 'client'
});

Router.route('run_search/:_id', {
  name: 'runSearch',
  controller: 'RunSearchController',
  action: 'action',
  where: 'client'
});